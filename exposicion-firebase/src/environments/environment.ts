// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'exposicion-2f4bf',
    appId: '1:143761421881:web:9fda741cac615dc1e73fe8',
    storageBucket: 'exposicion-2f4bf.appspot.com',
    apiKey: 'AIzaSyCPG3k9LXnjQwjc6g7R2VPyQYRbB82JK0w',
    authDomain: 'exposicion-2f4bf.firebaseapp.com',
    messagingSenderId: '143761421881',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
